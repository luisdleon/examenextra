import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {AuthService} from '../services/auth.service'

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
public email: string;
public password: string;
  constructor(public router: Router,
    public authService: AuthService) { }

  ngOnInit() {
  }
  onSubmitLogUser(){
    this.authService.loginEmail(this.email,this.password)
    .then ((res)=>{
      this.router.navigate(['/pacientes']);
    }) .catch ((err)=>{
      console.log(err);
      this.router.navigate(['/login']);

    })
    
    
      }
}
