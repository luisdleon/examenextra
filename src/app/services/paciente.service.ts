import { Injectable } from '@angular/core';
import {AngularFireDatabase,AngularFireList, AngularFireDatabaseModule} from 'angularfire2/database'
import { Paciente } from '../models/paciente';


@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  clientList: AngularFireList<any>;
  selectedPaciente: Paciente = new Paciente();

  constructor(private firebase: AngularFireDatabase) { }


  getPacientes()
  {
    return this.clientList=this.firebase.list('pacientes');
  }

  insertPaciente(paciente: Paciente)
  {
    this.clientList.push({
      name: paciente.name,
      apellido:paciente.apellido,
      doctor: paciente.doctor,
      emergencia: paciente.emergencia
    });
  }
  updatePaciente (paciente: Paciente)
  {
    this.clientList.update(paciente.$key, {
      name: paciente.name,
      apellido: paciente.apellido,
      doctor: paciente.doctor,
      emergencia: paciente.emergencia
    });
  }

  deletePaciente($key: string){
    this.clientList.remove($key);
  }
}
