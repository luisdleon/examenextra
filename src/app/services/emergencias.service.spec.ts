import { TestBed } from '@angular/core/testing';

import { EmergenciasService } from './emergencias.service';

describe('EmergenciasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmergenciasService = TestBed.get(EmergenciasService);
    expect(service).toBeTruthy();
  });
});
