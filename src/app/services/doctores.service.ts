import { Injectable } from '@angular/core';
import {AngularFireDatabase,AngularFireList, AngularFireDatabaseModule} from 'angularfire2/database'
import { Doctores } from '../models/doctores';


@Injectable({
  providedIn: 'root'
})
export class DoctoresService {

  doctorList: AngularFireList<any>;
  selectedDoctores: Doctores = new Doctores();

  constructor(private firebase: AngularFireDatabase) { }


  getDoctores()
  {
    return this.doctorList=this.firebase.list('doctores');
  }

  insertDoctores(Doctores: Doctores)
  {
    this.doctorList.push({
      nombre: Doctores.nombre,
      apellido:Doctores.apellido,
      telefono: Doctores.telefono,
      edad: Doctores.edad,
      colonia: Doctores.colonia,
      tienda: Doctores.tienda,
      municipio: Doctores.municipio,
      estado: Doctores.estado,
    });
  }
  updateDoctores (Doctores: Doctores)
  {
    this.doctorList.update(Doctores.$key, {
      nombre: Doctores.nombre,
      apellido:Doctores.apellido,
      telefono: Doctores.telefono,
      edad: Doctores.edad,
      colonia: Doctores.colonia,
      tienda: Doctores.tienda,
      municipio: Doctores.municipio,
      estado: Doctores.estado,

    });
  }

  deleteDoctores($key: string){
    this.doctorList.remove($key);
  }

  
}
