import { Injectable } from '@angular/core';
import {AngularFireDatabase,AngularFireList, AngularFireDatabaseModule} from 'angularfire2/database'
import { Emergencias } from '../models/emergencias';


@Injectable({
  providedIn: 'root'
})
export class EmergenciasService {

  emergenciaList: AngularFireList<any>;
  selectedEmergencias: Emergencias = new Emergencias();

  constructor(private firebase: AngularFireDatabase) { }


  getEmergencias()
  {
    return this.emergenciaList=this.firebase.list('Emergencias');
  }

  insertEmergencias(Emergencias: Emergencias)
  {
    this.emergenciaList.push({
      name: Emergencias.name,
      apellido:Emergencias.apellido,
      doctor:Emergencias.doctor,
      emergencia: Emergencias.emergencia,
      fecha: Emergencias.fecha
    });
  }
  updateEmergencias (Emergencias: Emergencias)
  {
    this.emergenciaList.update(Emergencias.$key, {
      name: Emergencias.name,
      apellido: Emergencias.apellido,
      doctor: Emergencias.doctor,
      emergencia: Emergencias.emergencia,
      fecha: Emergencias.fecha
    });
  }

  deleteEmergencias($key: string){
    this.emergenciaList.remove($key);
  }
}
