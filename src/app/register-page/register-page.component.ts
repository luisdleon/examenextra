import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import { NgForm } from '@angular/forms';
import { Registro } from '../models/registro';
import {Router} from '@angular/router'

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})

export class RegisterPageComponent implements OnInit {
public email: string;
public password: string;
public nombre: string;
public apellido: string;
public direccion: string;


  constructor( public authService: AuthService, 
    public router: Router ) { }

  ngOnInit() {
    this.authService.getAuth();
    this.resetForm();
  }
  onSubmitAddUser(registroform: NgForm){
this.authService.registerUser(this.email, this.password)

this.resetForm(registroform);
this.router.navigate(['/pacientes']);

  }
  resetForm(pacienteform?: NgForm)
{
  if(pacienteform != null)
  pacienteform.reset();
  this.authService.selectedRegistro = new Registro ();
  
}

}
