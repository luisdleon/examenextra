import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';
import { Doctores } from 'src/app/models/doctores';
import { DoctoresService } from 'src/app/services/doctores.service';
  

@Component({
  selector: 'app-doctores',
  templateUrl: './doctores.component.html',
  styleUrls: ['./doctores.component.css']
})
export class DoctoresComponent implements OnInit {

  constructor(private doctoresService: DoctoresService) { }

  ngOnInit() {
    this.doctoresService.getDoctores();
    this.resetForm();
  }
  onSubmit(doctoresform: NgForm)
  {
    if (doctoresform.value.$key == null)
    this.doctoresService.insertDoctores(doctoresform.value)
  else 
this.doctoresService.updateDoctores(doctoresform.value);
this.resetForm(doctoresform);
  }
resetForm(doctoresform?: NgForm)
{
  if(doctoresform != null)
  doctoresform.reset();
  this.doctoresService.selectedDoctores = new Doctores ();
}

}

