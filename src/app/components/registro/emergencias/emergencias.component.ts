import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import { Emergencias } from 'src/app/models/emergencias';
import { EmergenciasService } from 'src/app/services/emergencias.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-emergencias',
  templateUrl: './emergencias.component.html',
  styleUrls: ['./emergencias.component.css']
})
export class EmergenciasComponent implements OnInit {
 
  constructor(private EmergenciasService: EmergenciasService) { }

  ngOnInit() {
    this.EmergenciasService.getEmergencias();
    this.resetForm();
  }
  onSubmit(emergenciasform: NgForm)
  {
    if (emergenciasform.value.$key == null)
    this.EmergenciasService.insertEmergencias(emergenciasform.value)
  else 
this.EmergenciasService.updateEmergencias(emergenciasform.value);
this.resetForm(emergenciasform);
  }
resetForm(emergenciasform?: NgForm)
{
  if(emergenciasform != null)
  emergenciasform.reset();
  this.EmergenciasService.selectedEmergencias = new Emergencias ();
}

}



