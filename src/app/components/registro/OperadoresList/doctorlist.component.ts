import { Component, OnInit } from '@angular/core';
import {DoctoresService} from '../../../services/doctores.service'
import {Doctores} from '../../../models/doctores'
import { element } from '@angular/core/src/render3';
@Component({
  selector: 'app-doctorlist',
  templateUrl: './doctorlist.component.html',
  styleUrls: ['./doctorlist.component.css']
})
export class DoctorlistComponent implements OnInit {

  headElements = ['Nombre', 'Apellido', 'Telefono', 'Colonia','Tienda','Municipio','Estado'];

  doctoresList: Doctores[]
  constructor(
    private doctoresService: DoctoresService) 
    { }

  ngOnInit() {
    this.doctoresService.getDoctores()
    .snapshotChanges()
    .subscribe(item => {
    this.doctoresList =[];
    item.forEach(element => {
    let x = element.payload.toJSON();
    x["$key"] = element.key;
    this.doctoresList.push(x as Doctores);
    
    })

    })
  }

  onEdit(doctores: Doctores){
this.doctoresService.selectedDoctores = Object.assign({},doctores);
  }
  onDelete($key: string){
this.doctoresService.deleteDoctores($key);
  }
  
}
