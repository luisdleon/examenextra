import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergenciaslistComponent } from './emergenciaslist.component';

describe('EmergenciaslistComponent', () => {
  let component: EmergenciaslistComponent;
  let fixture: ComponentFixture<EmergenciaslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergenciaslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergenciaslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
