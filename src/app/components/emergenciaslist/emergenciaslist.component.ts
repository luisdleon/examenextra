import { Component, OnInit } from '@angular/core';
import {DoctoresService} from '../../services/doctores.service'
import {Doctores} from '../../models/doctores'

@Component({
  selector: 'app-emergenciaslist',
  templateUrl: './emergenciaslist.component.html',
  styleUrls: ['./emergenciaslist.component.css']
})
export class EmergenciaslistComponent implements OnInit {
  
  headElements = ['Nombre', 'Apellido', 'Telefono', 'Colonia','Tienda','Municipio','Estado'];
  doctoresList: Doctores[]
  constructor(
    private doctoresService: DoctoresService) 
    { }

  ngOnInit() {
    this.doctoresService.getDoctores()
    .snapshotChanges()
    .subscribe(item => {
    this.doctoresList =[];
    item.forEach(element => {
    let x = element.payload.toJSON();
    x["$key"] = element.key;
    this.doctoresList.push(x as Doctores);
    
    })

    })
  }
}