import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DoctoresComponent } from './components/registro/RegistroOperadores/doctores.component';
import { EmergenciasComponent } from './components/registro/emergencias/emergencias.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { PacienteComponent } from './components/registro/Home/paciente.component';

const routes: Routes = [
  {path: 'login', component: LoginPageComponent},
  {path: 'pacientes', component: PacienteComponent},
  { path: 'doctores', component: DoctoresComponent},
  {path: 'emergencias', component: EmergenciasComponent},

  {path: 'registro', component: RegisterPageComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ PacienteComponent, DoctoresComponent,
  EmergenciasComponent,LoginPageComponent,RegisterPageComponent]