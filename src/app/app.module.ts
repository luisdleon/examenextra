import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatTableModule, MatButtonModule, MatToolbarModule, MatSidenavModule, MatListModule, MatPaginatorModule, MatSortModule, MatIconModule} from '@angular/material';



import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
//firebase
import {AngularFireDatabaseModule} from 'angularfire2/database'
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth'
import {environment} from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
//components
import { RegistroComponent } from './components/registro/registro.component';

//services
import {RouterModule, Routes } from '@angular/router'
import{PacienteService} from './services/paciente.service';
import { DoctorlistComponent } from './components/registro/OperadoresList/doctorlist.component';
import { EmergenciasComponent } from './components/registro/emergencias/emergencias.component';
import { PacienteComponent } from './components/registro/Home/paciente.component';
import { DoctoresComponent } from './components/registro/RegistroOperadores/doctores.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import {AuthService} from './services/auth.service';
import { EmergenciaslistComponent } from '../app/components/emergenciaslist/emergenciaslist.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';



@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    
    routingComponents,
    DoctorlistComponent,
    EmergenciasComponent,
    NavbarComponent,
    LoginPageComponent,
    RegisterPageComponent,
    EmergenciaslistComponent,
  
    PacienteComponent
    
  
  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatPaginatorModule,
    MatSortModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatIconModule,
   
   
    RouterModule.forRoot([
      {
        path: 'productos', 
        component: PacienteComponent
      },
      {
       
      
        path: 'doctores', 
        component: DoctoresComponent
      },
      {
        path: 'doctores/: listadoc',
        component: DoctorlistComponent
      },
      {
        path: 'emergencias',
        component: EmergenciasComponent
      },
      {
        path: 'emergencias/: listaemergencia',
        component: EmergenciaslistComponent
      }
    
    ]), 
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule
  ],
  providers: [

    PacienteService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
