// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyAr6-IfoZoslmw76zVbp7dz4nxF5H4g2Ew",
  authDomain: "fir-prueba-1c45b.firebaseapp.com",
  databaseURL: "https://fir-prueba-1c45b.firebaseio.com",
  projectId: "fir-prueba-1c45b",
  storageBucket: "fir-prueba-1c45b.appspot.com",
  messagingSenderId: "286231339173",
  appId: "1:286231339173:web:409322c65a04c6f1"
  }
  
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
